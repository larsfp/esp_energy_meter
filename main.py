# -*- coding: utf-8 -*-
# Copyright (C) 2017  Costas Tyfoxylos
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from umqtt.robust import MQTTClient
import ujson
import esp
import time
import utime
import machine
from ntptime import settime


def main():
    client_id = configuration.get('client_id')  # I.e. hostname
    topic_usage = client_id + configuration.get('topic_usage')  # MQTT topic
    topic_used = client_id + configuration.get('topic_used')  # MQTT topic
    exception_timeout = int(configuration.get('exception_reset_timeout'))
    mqtt_server_ip = configuration.get('mqtt_server_ip')
    mqtt_user=configuration.get('mqtt_user')
    mqtt_password=configuration.get('mqtt_password')
    measure_time = int(configuration.get('measure_time'))  # Set to 0 to publish instantly
    jitter = int(configuration.get('jitter'))  # Adjust according to your LDR value
    ppwh = configuration.get('ppwh')  # Pulse per watthour
    time_adjustment = configuration.get('time_adjustment')  # Time zone ish

    adc = machine.ADC(0)  # Analog pin
    led = machine.Pin(2, machine.Pin.OUT)  # Integrated LED
    c = MQTTClient(client_id, mqtt_server_ip, user=mqtt_user, password=mqtt_password)
    # c.set_last_will(client_id, 'disconnected')

    # Set time. There's currently no timezone support in MicroPython
    from ntptime import settime
    settime()

    try:
        c.connect()
        c.publish(client_id, 'online')

        delay = 0.01
        led.on()
        current = 0
        previous = 0
        rising = False
        count = 0
        previous_pulse = None

        while True:
            utc = utime.time()
            localtime = utc + time_adjustment

            (year, month, mday, hour, minute, second, weekday, yearday) = utime.localtime(localtime)
            if 0 == hour and 0 == minute and 0 == second:
                count = 0
                settime()
            #else:
            #    print("%s:%s:%s" % (hour, minute, second))

            # Read analog value from LDR
            previous = current
            current = adc.read()
            #print ("Current analog value: %s (diff %s)" % (current, (previous - current)))
            
            if current > previous + jitter: # Rising
                rising = True

            if current < previous and rising: # Falling after rising
                led.off()
                count += 1
                print ("Blink! (count %s)" % count)
                rising = False

                if 0 == measure_time or 0 == count%measure_time:  # Publish count (total usage) every ten blink
                    c.publish(topic_used, str(count))
                    if previous_pulse:
                        watt = round (3600 * 1000 / (utime.ticks_ms() - previous_pulse) / ppwh)
                        # Based on http://wolframalpha.com/input/?i=1+wh+in+0.5+second
                        # So we take 1W*1hr devided by seconds between pulses. Then devide on ppwh. Right?
                        c.publish(topic_usage, str(watt)) # Publish realtime W usage
                previous_pulse = utime.ticks_ms()
                led.on()

            time.sleep(delay)
    except Exception as e:
        print(('Caught exception, {}'
            'resetting in {} seconds...').format(e, exception_timeout))
        #c.disconnect()
        time.sleep(exception_timeout)
        machine.reset()


if __name__ == '__main__':
    main()
