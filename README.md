## Energy meter

Measure energy used by counting blinks on a power meter, using ESP8266, MicroPython and a Light Dependent Resistor (LDR).

Home: https://gitlab.com/larsfp/esp_energy_meter

This will publish two values via MQTT:
* count of blinks since boot i.e. ```esp10/energymeter/used 14```
* Realtime Watt usage based on time between blinks i.e. ```esp10/energymeter/usage 3600```

Count is reset at 00:00.

Time is updated via NTP at boot and 00:00.

Version 0.3. Working beta.


## Hardware

* Wemos D1 Pro Micro (or any board that will run MicroPython)
* An LDR
* A resistor (~300-10k)

Wire the LDR from 3,3V to A0. Wire the resistor from A0 to GND. Tape LDR in front of your light. Make sure it's shielded from other light sources.

![Bread board example](esp_energy_monitor_bb.png)


## TODO

* Find out where I got the original code for ESP, config, etc. for attribution
* Won't reconnect to wifi?
* Set last will? https://github.com/micropython/micropython-lib/blob/master/umqtt.simple/umqtt/simple.py
* Make sure actions at 00:00 only happens once.
* Semi-related: Integrate realtime pricing in hass? https://github.com/kipe/nordpool


## Push code to ESP

You need an ESP that supports MicroPyton http://docs.micropython.org/en/. I used https://wiki.wemos.cc/products:d1:d1_mini_pro. And you need the Adafruit MicroPython Tool https://github.com/adafruit/ampy installed on your workstation.

I assume you have MicroPython already installed on the board. Edit configuration.json and push files to ESP:

```bash
$ ampy --port=/dev/ttyUSB0 put main.py
$ ampy --port=/dev/ttyUSB0 put boot.py
$ ampy --port=/dev/ttyUSB0 put configuration.json
```

Reset the board, and the program should run.


## Tuning

Watch output of ```screen /dev/ttyUSB0 115200```:

```
...
Current analog value 6 (diff 0)
Current analog value 6 (diff 0)
Current analog value 7 (diff -1)
...
```

Your value may be in the tens like here, or in the hundreds, depending on the resistor used. Ajust "jitter" variable in config.json based on variations of this number when exposed to light.

If you give it light pulses at around 1 Hz the usage should show ~3600 W.


## Example Home-Assistant config

![Hass example](hass.png)

```yaml
- platform: mqtt
  state_topic: "esp10/energymeter/used"
  name: Energy used
  unit_of_measurement: 'Wh'
  expire_after: 120
  icon: mdi:power-plug
- platform: mqtt
  state_topic: "esp10/energymeter/usage"
  name: Energy usage
  unit_of_measurement: 'W'
  expire_after: 600
  icon: mdi:gauge

- platform: template
  sensors:
    cost_of_energy_today:
      friendly_name: Cost of energy today
      unit_of_measurement: 'NOK'
      value_template: "{{ ( states('sensor.energy_used')|float / 1000 * 1.06 ) | round(1) }}"
```

1.06 is the price pr kWh here.

## More pictures and graphs

![Installed](installation.jpg)

![Grafana example](grafana.png)
